package design.lanmu;

import design.lanmu.gui.MyJFrame;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @version v1.0
 * @Description: TODO(一句话描述该类的功能)
 * @Author: lanmu
 * @Date: 2020/10/17 20:59
 */
public class Main {

    static JTextField res = new JTextField("");

    static boolean expression = false;

    static char expressionStr;

    {
        Document document = res.getDocument();
        document.addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                System.out.println("1");
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                System.out.println("2");
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                System.out.println("3");
            }
        });
    }

    public static void appendTextFiled(String string) {
        String text = res.getText();
        res.setText(text += string);
    }

    public static void setRes(String str) {
        res.setText(str);
    }

    public static void setRes(Object str) {
        res.setText(str.toString());
    }

    public static JPanel generatorCalc() {
        JPanel panel=new JPanel();    //创建面板
        //指定面板的布局为GridLayout，4行4列，间隙为5
        panel.setLayout(new GridLayout(4,4,5,5));
        panel.add(generateClickButton("7"));    //添加按钮
        panel.add(generateClickButton("8"));
        panel.add(generateClickButton("9"));
        panel.add(generateClickButton("/", specialButtonListener("/")));
        panel.add(generateClickButton("4"));
        panel.add(generateClickButton("5"));
        panel.add(generateClickButton("6"));
        panel.add(generateClickButton("*", specialButtonListener("*")));
        panel.add(generateClickButton("1"));
        panel.add(generateClickButton("2"));
        panel.add(generateClickButton("3"));
        panel.add(generateClickButton("-", specialButtonListener("-")));
        panel.add(generateClickButton("0"));
        panel.add(generateClickButton("X", (e)->{
            String text = res.getText();
            if (text.length() >= 1) {
                int length = text.length();
                res.setText(text.substring(0, length - 1));
            }
        }));
        panel.add(generateClickButton("=", (event)->{
            String text = res.getText();
            String[] split = text.split("[+\\-*/]");
            if (split != null && split.length > 1) {
                int oneNumber = Integer.parseInt(split[0]);
                String twoNumber = split[1];
                if (twoNumber != null) {
                    int byCalcNumber = Integer.parseInt(twoNumber);
                    switch (expressionStr) {
                        case '+':
                            setRes(oneNumber + byCalcNumber);
                            break;
                        case '-':
                            setRes(oneNumber - byCalcNumber);
                            break;
                        case '*':
                            setRes(oneNumber * byCalcNumber);
                            break;
                        case '/':
                            setRes(oneNumber / byCalcNumber);
                            break;
                        default:
                            break;
                    }
                }
            }
            expression = false;
        }));
        panel.add(generateClickButton("+", specialButtonListener("+")));
//        panel.add(generateClickButton("X", (e) -> {
//            String text = res.getText();
//            res.setText(text.substring(0, text.length() - 2));
//        }));
        return panel;
    }

    public static ActionListener specialButtonListener(String str) {
        return (e) -> {
            notExistsInsertWithStr(str);
        };
    }

    public static void existsMoreSpecialStr() {
        char[] chars = {'+', '-', '*', '/'};

    }

    public static void notExistsInsertWithStr(String str) {
        if (expression) {
            return;
        }
        int i = res.getText().indexOf(str);
        if (i == -1) {
            expressionStr = str.charAt(0);
                    expression = true;
            appendTextFiled(str);
        }
    }

    public static void main(String[] args) {
        MyJFrame myJFrame = new MyJFrame("My 计算器");
        GridBagLayout gbaglayout=new GridBagLayout();    //创建GridBagLayout布局管理器
        GridBagConstraints constraints=new GridBagConstraints();
        myJFrame.setLayout(gbaglayout);    //使用GridBagLayout布局管理器
        constraints.fill=GridBagConstraints.BOTH;    //组件填充显示区域
        constraints.weightx=0.0;    //恢复默认值
        constraints.gridwidth = GridBagConstraints.REMAINDER;    //结束行

        gbaglayout.setConstraints(res, constraints);
        myJFrame.add(res);
        myJFrame.add(generatorCalc());
    }

    public static JButton generateClickButton(String str) {
        JButton jButton = new JButton(str);
        jButton.setEnabled(true);
        jButton.addActionListener((e) -> {
            String text = jButton.getText();
            String text1 = res.getText();
            res.setText(text1 + text);
        });
        return jButton;
    }

    public static JButton generateClickButton(String str, ActionListener actionListener) {
        JButton jButton = new JButton(str);
        jButton.setEnabled(true);
        jButton.addActionListener(actionListener);
        return jButton;
    }

}
