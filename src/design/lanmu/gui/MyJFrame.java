package design.lanmu.gui;

import javax.swing.*;
import java.awt.*;

/**
 * @version v1.0
 * @Description: TODO(一句话描述该类的功能)
 * @Author: lanmu
 * @Date: 2020/10/17 21:01
 */
public class MyJFrame extends JFrame {

    public MyJFrame(String name) {
        super(name);
        setVisible(true);
        setBounds(300, 100, 400, 200);
        setBackground(Color.WHITE);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public MyJFrame() {
        this("");
    }


}
